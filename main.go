package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"runtime/pprof"
)

func main() {
	dir := flag.String("d", "", "Directory")
	outfile := flag.String("out", "", "Outfile")
	profiling := flag.Bool("p", false, "Profiling")

	flag.Parse()

	if *profiling {
		f, err := os.Create("cpuprofile")
		if err != nil {
			panic(fmt.Errorf("could not create memory profile: %s", err))
		}
		defer f.Close()

		err = pprof.StartCPUProfile(f)
		if err != nil {
			panic(fmt.Errorf("failed to start cpu profile: %s", err))
		}

		defer pprof.StopCPUProfile()
	}

	if *dir == "" {
		panic("flag d is empty")
	}

	if *outfile == "" {
		panic("flag out is empty")
	}

	file, err := os.Create(*outfile)
	if err != nil {
		panic(fmt.Errorf("could not create outfile: %s", err))
	}
	defer file.Close()

	scanDir(file, *dir, "")
}

func scanDir(fl *os.File, path string, indent string) {

	files, err := ioutil.ReadDir(path)
	if err != nil {
		panic(fmt.Errorf("could not reading dir %s: %s", path, err))
	}

	for _, file := range files {

		fullPath := filepath.Join(path, file.Name())

		if file.IsDir() {
			fmt.Fprintf(fl, "%s%s/\n", indent, file.Name())
			scanDir(fl, fullPath, indent+"  ")
		} else {
			fmt.Fprintf(fl, "%s%s\n", indent, file.Name())
		}
	}

}
